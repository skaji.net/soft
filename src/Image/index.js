import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ImageWrapper = styled.img`
  height: ${({width}) => width}px;
  object-fit: ${({width}) => 'contain'};
  width: ${({width}) => width}px;
`

const Image = ({ ...props }) => {
  return <ImageWrapper {...props} />
}

Image.propTypes = {
  src: PropTypes.any.isRequired,
  width: PropTypes.string
}

export default Image
