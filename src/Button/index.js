import styled from 'styled-components'

const Button = styled.button`
  cursor: pointer;
  border: none;
  background: transparent;
  flex: 0 0 auto;
`

export default Button
