import React, {Component} from 'react'
import styled from 'styled-components'
import {POKEMON_DATA} from './Data/index'
import arrow from './arrow.png'
import arrowBack from './arrow-back.png'
import arrowForward from './arrow-forward.png'
import Image from './Image'
import Button from './Button'
import FullInfo from './FullInfo'

const PageBody = styled.div`
  padding: 60px;
  height: calc(100vh - 133px);
`
const Pagination = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`
const Item = styled.div`
  margin: 5px;
  padding: 5px 10px;
  border: 2px solid #000;
  display: flex;
  justify-content: space-between;
`
const Name = styled.div`
  font-size: 16px;
  flex: 1 1 auto;
`
const Info = styled.div`
  display: flex;
  align-items: center;
`
const CounterPage = styled.div`
  font-size: 25px;
`
const Items = styled.div`
  height: 100%;
`
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

class App extends Component {
  constructor(props) {
    super(props)
    this.onOpenFullInfo = this.onOpenFullInfo.bind(this)
    this.onCloseFullInfo = this.onCloseFullInfo.bind(this)
    this.onClickDecrementCount = this.onClickDecrementCount.bind(this)
    this.onClickIncrementCount = this.onClickIncrementCount.bind(this)
  }
  
  state = {
    open: false,
    pokemon: '',
    count: 0
  }
  
  sliceArray(arr, size) {
    let result = []
    let len = arr.length
    for (let i = 0; i < len; i += size) {
      result.push(arr.slice(i, i + size))
    }
    return result
  }
  
  onOpenFullInfo(name) {
    this.setState({
      ...this.state,
      open: true,
      pokemon: name
    })
  }
  
  onCloseFullInfo() {
    this.setState({
      ...this.state,
      open: false
    })
  }
  
  onClickDecrementCount() {
    if (this.state.count > 0) {
      this.setState({
        ...this.state,
        count: this.state.count - 1
      })
    }
  }
  
  onClickIncrementCount() {
    if (this.state.count + 1 !== this.sliceArray(POKEMON_DATA, 3).length) {
      this.setState({
        ...this.state,
        count: this.state.count + 1
      })
    }
  }
  
  renderFullInfo = (name) => {
    for (let pokemon of POKEMON_DATA) {
      if (pokemon.name === name) {
        return <FullInfo {...pokemon} onCloseFullInfo={this.onCloseFullInfo}/>
      }
    }
  }
  
  render() {
    const {open, pokemon, count} = this.state
    
    return (
      <PageBody>
        {!open && (
          <Wrapper>
            <Items>
              {POKEMON_DATA && this.sliceArray(POKEMON_DATA, 3)[count].map((item, key) => (
                <Item key={key}>
                  <Info>
                    <Name>
                      {item.name}
                    </Name>
                  </Info>
                  <Button onClick={() => this.onOpenFullInfo(item.name)}>
                    <Image src={arrow}/>
                  </Button>
                </Item>
              ))}
            </Items>
            {!open && (
              <Pagination>
                <Button onClick={this.onClickDecrementCount}>
                  <Image src={arrowBack}/>
                </Button>
                <CounterPage>
                  Page {this.state.count + 1} of {this.sliceArray(POKEMON_DATA, 3).length}
                </CounterPage>
                <Button onClick={this.onClickIncrementCount}>
                  <Image src={arrowForward}/>
                </Button>
              </Pagination>
            )}
          </Wrapper>
        )}
        {open && this.renderFullInfo(pokemon)}
      </PageBody>
    );
  }
}

export default App;
