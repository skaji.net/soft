import React, {Component} from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import arrow from '../arrow-back.png'
import Button from '../Button'
import Image from '../Image'

const Name = styled.div`
  flex: 1 1 auto;
  font-size: 70px;
  text-align: center;
  font-weight: bold;
`
const Heading = styled.div`
  display: flex;
  align-items: center;
`
const StatsWrapper = styled.div`

`
const ImageWrapper = styled.div`
  text-align: center;
`
const HeadingStats = styled.div`
  font-size: 36px;
  padding: 20px;
  text-align: center;
`
const Stats = styled.div`
  text-align: center;
  font-size: 16px;
`

class FullInfo extends Component {
  constructor(props) {
    super(props)
    this.onCloseFullInfo = this.onCloseFullInfo.bind(this)
  }
  
  static propTypes = {
    name: PropTypes.string,
    height: PropTypes.any,
    weight: PropTypes.any,
    speed: PropTypes.any,
    attack: PropTypes.any,
    defense: PropTypes.any,
    hp: PropTypes.any,
    onCloseFullInfo: PropTypes.func
  }
  
  onCloseFullInfo() {
    this.props.onCloseFullInfo()
  }
  
  renderImage = (name) => {
    function importAll(r) {
      let images = {}
      r.keys().map((item) => {
        images[item.replace('./', '')] = r(item)
      })
      return images;
    }
    
    const images = importAll(require.context('../Images', false, /\.(png|jpe?g|svg)$/));
    //еще можно по значению искать картинку в интернете и ее брать, что бы не хранить у себя, это сложное решение и требует еще времени, но мысль не плохая
    
    switch (name) {
      case name:
        return (
          <ImageWrapper>
            <Image width="400" src={images[`${name}.jpg`]}/>
          </ImageWrapper>
        )
      default:
        return null
    }
  }
  
  render() {
    const {name, height, weight, speed, attack, defense, hp} = this.props
    
    return (
      <div>
       <Heading>
         <Button onClick={this.onCloseFullInfo}>
           <Image src={arrow} />
         </Button>
         <Name>{name}</Name>
       </Heading>
        {this.renderImage(name)}
        <StatsWrapper>
          <HeadingStats>
            Stats
          </HeadingStats>
          <Stats>height: {height}</Stats>
          <Stats>weight: {weight}</Stats>
          <Stats>speed: {speed}</Stats>
          <Stats>attack: {attack}</Stats>
          <Stats>defense: {defense}</Stats>
          <Stats>hp: {hp}</Stats>
        </StatsWrapper>
      </div>
    )
  }
}

export default FullInfo
